import { createStore, applyMiddleware, combineReducers } from "redux";
import products from "./products";
import thunk from "redux-thunk";

const middleware = [thunk];

const store = createStore(
  combineReducers({
    products: products
  }),
  applyMiddleware(...middleware)
);

export default store;
