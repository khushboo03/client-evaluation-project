import {
  PRODUCT_LIST,
  PRODUCT_ADD_TO_CART,
  PRODUCT_REMOVE_FROM_CART
} from "../actions/types";
import jsonProducts from "../product.json";

const productList = jsonProducts.filter(product => {
  return product.isPublished === "true";
});

export default function products(
  state = { productList: productList, cartProductList: [] },
  action = {}
) {
  switch (action.type) {
    case PRODUCT_LIST:
      return {
        ...state,
        items: action
      };

    case PRODUCT_ADD_TO_CART:
      let productAddedToCart = action.payload;
      let newProductList = state.productList.filter(
        product => product.productImage !== productAddedToCart.productImage
      );
      let newProductAddedToCart = [
        ...state.cartProductList,
        productAddedToCart
      ];

      return {
        ...state,
        cartProductList: newProductAddedToCart,
        productList: newProductList
      };

    case PRODUCT_REMOVE_FROM_CART:
      let productRemovedFromCart = action.payload;
      let newCartList = state.cartProductList.filter(
        product => product.productImage !== productRemovedFromCart.productImage
      );
      let newProductsList = [...state.productList, productRemovedFromCart];

      return {
        ...state,
        cartProductList: newCartList,
        productList: newProductsList
      };

    default:
      return state;
  }
}
