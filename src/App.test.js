import {shallow} from 'enzyme';
import React from 'react';
import App from './App'

describe('<App>',()=>{
  it('renders I App component', () => {
    const component = shallow(<App/>);
    expect(component).toHaveLength(1);
  })
});
