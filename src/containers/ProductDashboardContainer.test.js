import React from "react";
import { shallow } from "enzyme";
import {mapDispatchToProps, mapStateToProps} from "./ProductDashboardContainer";
import {addProductToCart} from '../actions/productAction'
import {PRODUCT_ADD_TO_CART} from '../actions/types'

const ownProps = {};
const state = {
products: {
    productList: [
    {
        isPublished: "true",
        productName: "Apple iPhone X",
        productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
        price: "299"
    },
    {
        isPublished: "true",
        productName: "Apple iPhone X",
        productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
        price: "299"
    }
    ],
    cartProductList: [
    {
        isPublished: "true",
        productName: "Apple iPhone X",
        productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
        price: "299"
    }
    ]
}
};

describe('<ProductDashboardContainer/>', () => {
  describe('mapStateToProps', () => {
    it('fetches product list and cart products from state and creates props', ()=>{
        const props = mapStateToProps(state, ownProps);
        expect(props.productList.length).toEqual(2)
    });
  });
  describe('mapDispatchToProps', () => {
    it('dispathches an action to addProduct to cart when addProductToCart is called', ()=>{
        const addProductToCart = jest.fn();
        const props = mapDispatchToProps(addProductToCart);
        props.addProductToCart(state.products.productList[0]);
        expect(addProductToCart).toHaveBeenCalled()
    });

    it('dispathches an action to addProduct to cart when addProductToCart is called', ()=>{
        const removeProductFromCart = jest.fn();
        const props = mapDispatchToProps(removeProductFromCart);
        props.removeProductFromCart(state.products.productList[0]);
        expect(removeProductFromCart).toHaveBeenCalled()
    });
  });
  
});