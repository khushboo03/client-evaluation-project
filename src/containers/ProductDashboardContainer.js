import { connect } from "react-redux";
import ProductDashboard from "../components/ProductDashboard";
import {addProductToCart, removeProductFromCart} from "../actions/productAction";

export const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    productList: state.products.productList,
    cartProductList: state.products.cartProductList
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    addProductToCart: product => {
      dispatch(addProductToCart(product));
    },
    removeProductFromCart: product => {
      dispatch(removeProductFromCart(product));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDashboard);
