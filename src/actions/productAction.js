import {
  PRODUCT_LIST,
  PRODUCT_ADD_TO_CART,
  PRODUCT_REMOVE_FROM_CART
} from "./types";
import products from "../product.json";

export const fetchProductList = () => dispatch => {
  dispatch({
    type: PRODUCT_LIST,
    payload: products
  });
};

export const addProductToCart = product => dispatch => {
  dispatch({
    type: PRODUCT_ADD_TO_CART,
    payload: product
  });
};

export const removeProductFromCart = product => dispatch => {
  dispatch({
    type: PRODUCT_REMOVE_FROM_CART,
    payload: product
  });
};
