import React, { Component } from "react";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store";
import ProductDashboardContainer from "./containers/ProductDashboardContainer";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ProductDashboardContainer />
      </Provider>
    );
  }
}

export default App;
