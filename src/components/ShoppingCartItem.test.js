import React from "react";
import { shallow } from "enzyme";
import ShoppingCartItem from "./ShoppingCartItem";

describe("<ShoppingCartItem/>", () => {
    it('display product item added to cart', () => {
        const props = { 
            addedProduct:{
             productName: "Iphone",
             productImage: "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png" 
            }
           };
           const wrapper = shallow(<ShoppingCartItem {...props} />);
           expect(wrapper.find('.product-image').prop('src')).toEqual(props.addedProduct.productImage);
           expect(wrapper).toContainComponentWithProps('.product-name',{children:'Iphone'});
           expect(wrapper).toContainComponentWithProps('.removeItemButton',{children: 'Remove'});
           
    })
})