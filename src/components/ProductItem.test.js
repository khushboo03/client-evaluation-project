import React from 'react';
import { shallow } from 'enzyme';
import ProductItem from './ProductItem';

describe('ProductItem component' ,() => {
    it('renders a list item', () => {
      const props = { 
       product:{
        productName: "Iphone", 
        productImage: '"https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png"', 
        price: "99"
       }
      };
      const wrapper = shallow(<ProductItem {...props} />);
      expect(wrapper).toContainComponentWithProps('h5',{children:'Iphone'});
      expect(wrapper).toContainComponentWithProps('p',{children: ['$','99']});
      expect(wrapper).toContainComponentWithProps('button',{children: 'Add to cart'});
    });
})