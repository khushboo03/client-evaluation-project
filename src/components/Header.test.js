import React, { Component } from "react";
import { shallow } from "enzyme";
import Header from "./Header";

describe("<Header/>", () => {
  const props = {
    cartFlag: true,
    cartProductList: [
      {
        isPublished: "true",
        productName: "Apple iPhone X",
        productImage:
          "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
        price: "299"
      },
      {
        isPublished: "true",
        productName: "Apple iPhone 8",
        productImage:
          "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-8/iphone-8-silver-grid.png",
        price: "100"
      }
    ]
  };
  it("should render Product-Items when cart-flag is true", () => {
    const wrapper = shallow(<Header {...props} />);

    expect(wrapper.find(".head-bar").text()).toEqual("Product-Items");
    expect(wrapper.find(".header-label > div > label").text()).toEqual(
      "Shopping-Cart"
    );
  });
});

describe("<Header/>", () => {
  const props = {
    cartFlag: false,
    cartProductList: [
      {
        isPublished: "true",
        productName: "Apple iPhone X",
        productImage:
          "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
        price: "299"
      },
      {
        isPublished: "true",
        productName: "Apple iPhone 8",
        productImage:
          "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-8/iphone-8-silver-grid.png",
        price: "100"
      }
    ]
  };
  it("should render Shopping-Cart when cart-flag is false", () => {
    const wrapper = shallow(<Header {...props} />);
    expect(wrapper.find(".head-bar").text()).toEqual("Shopping-Cart");
    expect(wrapper.find(".header-label > label").text()).toEqual(
      "Continue Shopping ..."
    );
  });
});
