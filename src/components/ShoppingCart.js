import React, { Component } from "react";
import ShoppingCartItem from "./ShoppingCartItem";

class ShoppingCart extends Component {
  render() {
    console.log(this.props.cartProductList);
    return (
      <section className="col-12 shoppingCart">
        <div className="row shoppingList">
          <div className="container">
            {this.props.cartProductList.length > 0 ? (
              <ul className="addedItem">
                {this.props.cartProductList.map(product => (
                  <ShoppingCartItem
                    addedProduct={product}
                    key={product.productName}
                    handleRemoveProductFromCart={item =>
                      this.props.handleRemoveProductFromCart(item)
                    }
                  />
                ))}
              </ul>
            ) : (
              <h3 className="cart-empty-message">
                Your cart is Empty <i className="far fa-frown" />
              </h3>
            )}
          </div>
        </div>
      </section>
    );
  }
}
export default ShoppingCart;
