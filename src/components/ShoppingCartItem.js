import React, { Component } from "react";
import "../App.css";

class ShoppingCartItem extends Component {
  render() {
    return (
      <li>
        <p className="list-cart">
          <span >
          <img className="product-image"
            src={this.props.addedProduct.productImage}
            style={{ width: "2%" , float: "left"}}
            alt="mobile"
          />
          <label className="product-name">
            {this.props.addedProduct.productName}
          </label>
          </span>
          <span>
            <button 
              className="removeItemButton btn btn-danger"
              onClick={e =>
                this.props.handleRemoveProductFromCart(this.props.addedProduct)
              }
            >
              Remove
            </button>
          </span>
          </p>
      </li>
    );
  }
}

export default ShoppingCartItem;
