import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <header>
        <div className="row title-head">
          <div className="col-sm header-label head-bar">
            {this.props.cartFlag ? (
              <h3>Product-Items</h3>
            ) : (
                <h3>Shopping-Cart</h3>
              )}
          </div>
          <div className="col-sm header-label">
            {this.props.cartFlag ? (
              <div onClick={this.props.handleShoppingCart}>
                <label className="float-right label-cart only-lg-devices">
                  Shopping-Cart
                </label>
                <i
                  className="fa fa-shopping-cart float-right"
                >
                  {this.props.cartProductList.length}
                </i>
              </div>
            ) : (
                <label
                  className="float-right label-cart"
                  onClick={this.props.handleShoppingCart}
                >
                  Continue Shopping ...
              </label>
              )}
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
