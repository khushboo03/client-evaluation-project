import React, { Component } from "react";
import ProductList from "./ProductList";
import ShoppingCart from "./ShoppingCart";
import Header from "./Header";
import "../App.css";

class ProductDashboard extends Component {
  constructor(props) {
    super(props);
    this.handleAddProductToCart = this.handleAddProductToCart.bind(this);
    this.handleRemoveProductFromCart = this.handleRemoveProductFromCart.bind(this);
    this.handleShoppingCart = this.handleShoppingCart.bind(this);

    this.state = {showProductListflag: true}
  }

  handleShoppingCart() {
    this.state.showProductListflag ? this.setState({showProductListflag: false}) : this.setState({showProductListflag: true})
  }

  handleAddProductToCart(item) {
    this.props.addProductToCart(item);
  }

  handleRemoveProductFromCart(item) {
    this.props.removeProductFromCart(item);
  }

  render() {
    const { productList, cartProductList } = this.props;

    return (
      <div className="sampleShop">
        <Header cartProductList={cartProductList} handleShoppingCart={this.handleShoppingCart} cartFlag = {this.state.showProductListflag}/>
        <div className="row">
         
         {this.state.showProductListflag && <ProductList
            mobileProducts={productList}
            handleAddProductToCart={this.handleAddProductToCart} 
          />}
         
       {!this.state.showProductListflag && <ShoppingCart
            cartProductList={cartProductList}
            handleRemoveProductFromCart={this.handleRemoveProductFromCart}
          />}
        </div>
      </div>
    );
  }
}

export default ProductDashboard;
