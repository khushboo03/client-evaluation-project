import React, { Component } from "react";
import { shallow } from "enzyme";
import ProductDashboard from "./ProductDashboard";

const props = {
  showProductListflag: false,
  productList: [
    {
      isPublished: "true",
      productName: "Apple iPhone X",
      productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
      price: "299"
    },
    {
      isPublished: "true",
      productName: "Apple iPhone X",
      productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
      price: "299"
    }
  ],
  cartProductList: [
    {
      isPublished: "true",
      productName: "Apple iPhone X",
      productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
      price: "299"
    }
  ]
};

describe("<ProductDashboard/>", () => {
  const wrapper = shallow(<ProductDashboard {...props} />);
  it("renders ProductList component", () => {
    if (props.showProductListflag == true) {
      expect(wrapper.find("ProductList")).toExist();
      expect(wrapper.find("ShoppingCart")).not.toExist();
    }
  });
});
