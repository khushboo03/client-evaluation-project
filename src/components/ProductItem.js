import React, { Component } from "react";
import "../App.css";

class ProductItem extends Component {
  render() {
    return (
      <div className="col-md-3 productItemBox rounded border border-info">
        <div className="Item">
          <h5>{this.props.product.productName}</h5>
          <img
            src={this.props.product.productImage}
            style={{ width: "40%", height: "30%" }}
            alt="mobile"
          />
          <p className="amount">${this.props.product.price}</p>
          <button type="button"
            className="addToCart btn btn-info"
            onClick={e => this.props.handleAddProductToCart(this.props.product)}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
export default ProductItem;
