import React from "react";
import { shallow } from "enzyme";
import ShoppingCart from "./ShoppingCart";
import ShoppingCartItem from "./ShoppingCartItem";

const props = {
  cartProductList: [
    {
      isPublished: "true",
      productName: "Apple iPhone X",
      productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-x/iphone-x-silver-grid.png",
      price: "299"
    },
    {
      isPublished: "true",
      productName: "Apple iPhone 8",
      productImage:
        "https://www.telstra.com.au/content/dam/tcom/personal/mobile-phones/product-catalogue/iphone-8/iphone-8-silver-grid.png",
      price: "100"
    }
  ]
};

describe("<ShoppingCart />", () => {
  it("renders five shoppingCartItem inside ShoppingCart components", () => {
    const wrapper = shallow(<ShoppingCart {...props} />);
    expect(wrapper.find("ShoppingCartItem")).toExist();
    expect(wrapper.find("ShoppingCartItem").length).toEqual(2);
  });
  it("it displays empty product list message when no products to display", () => {
    const props = {
      cartProductList: []
    };
    const wrapper = shallow(<ShoppingCart {...props} />);
    expect(wrapper.find(".cart-empty-message").text()).toEqual(
      "Your cart is Empty "
    );
  });
});
