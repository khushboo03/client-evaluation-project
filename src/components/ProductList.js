import React, { Component } from "react";
import ProductItem from "./ProductItem";

class ProductList extends Component {
  render() {
    const mobileProducts = this.props.mobileProducts;
    return (
      <div className="col-12 col-md-12">
        <section className="productLists">
          <div className="container productItems">
            <div className="row">
              {mobileProducts.map(item => (
                <ProductItem
                  className="product-item"
                  product={item}
                  key={item.productName}
                  handleAddProductToCart={item =>
                    this.props.handleAddProductToCart(item)
                  }
                />
              ))}
            </div>
            {!mobileProducts.length && (
              <h3 className="cart-empty-message">
                Yeah !!! you got all items in your cart
                <i className="far fa-smile" />
              </h3>
            )}
          </div>
        </section>
      </div>
    );
  }
}

export default ProductList;
